/**
 * This is copied from the `bitcoincash` crate, where it is not exported.
 */
#[macro_export]
macro_rules! impl_nexa_consensus_encoding {
    ($thing:ident, $($field:ident),+) => (
        impl bitcoincash::consensus::Encodable for $thing {
            #[inline]
            fn consensus_encode<S: std::io::Write>(
                &self,
                mut s: S,
            ) -> Result<usize, std::io::Error> {
                let mut len = 0;
                $(len += self.$field.consensus_encode(&mut s)?;)+
                Ok(len)
            }
        }

        impl bitcoincash::consensus::Decodable for $thing {
            #[inline]
            fn consensus_decode<D: std::io::Read>(
                d: D,
            ) -> Result<$thing, bitcoincash::consensus::encode::Error> {
                let mut d = d.take(bitcoincash::consensus::encode::MAX_VEC_SIZE as u64);
                Ok($thing {
                    $($field: bitcoincash::consensus::Decodable::consensus_decode(&mut d)?),+
                })
            }
        }
    );
}

/**
 * This is copied from the `bitcoincash` crate, where it is not exported.
 */
#[macro_export]
macro_rules! impl_nexa_hashencode {
    ($hashtype:ident) => {
        impl bitcoincash::consensus::Encodable for $hashtype {
            fn consensus_encode<S: std::io::Write>(&self, s: S) -> Result<usize, std::io::Error> {
                self.0.consensus_encode(s)
            }
        }

        impl bitcoincash::consensus::Decodable for $hashtype {
            fn consensus_decode<D: std::io::Read>(
                d: D,
            ) -> Result<Self, bitcoincash::consensus::encode::Error> {
                use bitcoincash::hashes::Hash;
                Ok(Self::from_inner(
                    <<$hashtype as bitcoincash::hashes::Hash>::Inner>::consensus_decode(d)?,
                ))
            }
        }
    };
}
