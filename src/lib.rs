#![recursion_limit = "1024"]
#![cfg_attr(feature = "fail-on-warnings", deny(warnings))]

#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_json;
extern crate serde;
#[macro_use]
extern crate configure_me;

extern crate jemallocator;
#[global_allocator]
static ALLOC: jemallocator::Jemalloc = jemallocator::Jemalloc;

#[macro_use]
extern crate bitcoin_hashes;

pub mod app;
pub mod bulk;
pub mod cache;
pub mod cashaccount;
pub mod chaindef;
pub mod config;
pub mod daemon;
pub mod def;
pub mod doslimit;
pub mod encode;
pub mod errors;
pub mod fake;
pub mod index;
pub mod indexes;
pub mod mempool;
pub mod metrics;
pub mod nexa;
pub mod query;
pub mod rndcache;
pub mod rpc;
pub mod scripthash;
pub mod signal;
pub mod store;
pub mod timeout;
pub mod util;
pub mod utilnumber;
pub mod wstcp;
