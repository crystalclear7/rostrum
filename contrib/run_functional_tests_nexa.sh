#!/usr/bin/env bash

# Stop on failure
set -e

if [ -z "$BU_HOME" ]
then
    export BU_HOME=$HOME/NexaUnlimited
    echo "INFO: \$BU_HOME not set. Using $BU_HOME."
fi

if [ -z "$ROSTRUM_PATH" ]
then
    SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
    export ROSTRUM_PATH=$SCRIPT_DIR/../target/debug/rostrum
fi

export TEST_RUNNER=$BU_HOME/qa/pull-tester/rpc-tests.py
if [ ! -f "$TEST_RUNNER" ]
then
    # Check if compiled
    echo "ERROR: Did not find test runner at $TEST_RUNNER."
    false # stop script
fi

set -x # echo commands to terminal
export PYTHON_DEBUG=INFO
echo "(cd $BU_HOME; RUST_BACKTRACE=1 $TEST_RUNNER --electrum.exec=\"$ROSTRUM_PATH\" --electrum-only -parallel=0)"
(cd $BU_HOME; RUST_BACKTRACE=1 $TEST_RUNNER --electrum.exec="$ROSTRUM_PATH" --electrum-only)
