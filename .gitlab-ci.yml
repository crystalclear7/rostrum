variables:
  CARGO_HOME: $CI_PROJECT_DIR/cargo
image: dagurval/rostrum-buildenv

cache:
  key: $CI_JOB_NAME
  paths:
    - $CARGO_HOME
    - target
    - apt-cache
  when: always

.test_common:
  before_script:
    - rustc --version
    - cargo --version
    - export PATH=$PATH:$CARGO_HOME/bin
  artifacts:
    reports:
      junit: results.xml

.nightly_common:
  image: rustlang/rust:nightly-buster-slim
  before_script:
    - export APT_CACHE_DIR=`pwd`/apt-cache && mkdir -pv $APT_CACHE_DIR
    - apt-get update
    - apt-get
      -o dir::cache::archives="$APT_CACHE_DIR" install
      -y --no-install-recommends
      clang=1:7.* cmake=3.* libsnappy-dev=1.* curl make

    - export PATH=$PATH:$CARGO_HOME/bin
    - rustc --version
    - cargo --version
    - cargo install cargo2junit

.python_common:
  image: python:latest
  cache:
    key: $CI_JOB_NAME
    paths:
      - cache-pip
  variables:
    PIP_CACHE_DIR: $CI_PROJECT_DIR/cache-pip

#
# Builds Bitcoin Cash
#
build-stable:
  script:
    - cargo build --release --features=fail-on-warnings
  artifacts:
    paths:
      - target/release/rostrum

build-stable-no-default:
  before_script:
    - rustc --version
    - cargo --version
  script:
    - cargo build --features=fail-on-warnings --no-default-features
  artifacts:
    paths:
      - target/debug/rostrum

build-nightly:
  extends: .nightly_common
  script:
    - cargo build --features=fail-on-warnings
  artifacts:
    paths:
      - target/debug/rostrum
  allow_failure: true

build-docker:
  image: docker
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  cache: {}
  services:
    - docker:dind
  script:
    - docker build -t rostrum-app .

#
# Builds Nexa
#
build-stable-nexa:
  script:
    - cargo build --release --features=fail-on-warnings,nexa
  artifacts:
    paths:
      - target/release/rostrum

build-nightly-nexa:
  extends: .nightly_common
  script:
    - cargo build --features=fail-on-warnings,nexa
  artifacts:
    paths:
      - target/debug/rostrum
  allow_failure: true

build-docker-nexa:
  image: docker
  cache: {}
  services:
    - docker:dind
  script:
    - docker build --build-arg BUILD_FEATURES=nexa -t rostrum-app .
  rules:
    - if: $CI_COMMIT_BRANCH == "master"

#
# Linters
#
lint-code-style:
  before_script:
    - rustup component add rustfmt
  script:
    - cargo fmt --all -- --check

lint-clippy:
  before_script:
    - rustup component add clippy
  script:
    - cargo clippy -- -D warnings

lint-clippy-nexa:
  before_script:
    - rustup component add clippy
  script:
    - cargo clippy --features=nexa -- -D warnings

lint-bloat:
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  script:
    # Biggest functions
    - cargo bloat --release

    # Biggest dependencies
    - cargo bloat --release --crates

    # Longest to compile
    - cargo bloat --time -j 1

#
# Tests
#
test-stable:
  extends: .test_common
  script:
    - cargo test
        --features=fail-on-warnings
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml

test-stable-no-default:
  extends: .test_common
  script:
    - cargo test
        --features=fail-on-warnings
        --no-default-features
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml

test-nightly:
  extends:
    - .test_common
    - .nightly_common
  script:
    - cargo test
        --features=fail-on-warnings
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml
  allow_failure: true


test-stable-doc:
  extends: .test_common
  script:
    - cargo test
        --features=fail-on-warnings
        --doc
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml

#
# Tests Nexa
#
test-stable-nexa:
  extends: .test_common
  script:
    - cargo test
        --features=fail-on-warnings,nexa
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml

test-nightly-nexa:
  extends:
    - .test_common
    - .nightly_common
  script:
    - cargo test
        --features=fail-on-warnings,nexa
        --
        -Z unstable-options --format json --report-time
        | cargo2junit > results.xml
  allow_failure: true

#
# Published artifacts
#
generate-rustdoc:
  script:
    - cargo doc --no-deps -p rostrum
    - cargo rustdoc -p rostrum
  artifacts:
    paths:
      - target/doc

generate-mkdocs:
  extends: .python_common
  before_script:
    - pip install -q mkdocs
    - pip install -q mkdocs-material
    - pip install -q toml
  script:
    - ./contrib/build-docs.py
  artifacts:
    paths:
      - build-docs

archive-builds:
  extends: .python_common
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  needs: [build-stable]
  before_script:
    - pip install -q toml
  script:
    - ROSTRUM_VERSION=`./contrib/read-version-number.py`
    - GIT_HEAD=`git rev-parse --short HEAD`
    - ROSTRUM_VERSION="$ROSTRUM_VERSION-$GIT_HEAD"
    - mkdir -p output
    - gzip -c target/release/rostrum > "output/rostrum-$ROSTRUM_VERSION.gz"
    - echo
      "<html><body><a href=\"rostrum-$ROSTRUM_VERSION.gz\">
        rostrum-$ROSTRUM_VERSION.gz
      </a></body></html>"
      > output/index.html
  artifacts:
    paths:
      - output

archive-builds-nexa:
  extends: .python_common
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  needs: [build-stable-nexa]
  before_script:
    - pip install -q toml
  script:
    - ROSTRUM_VERSION=`./contrib/read-version-number.py`
    - GIT_HEAD=`git rev-parse --short HEAD`
    - ROSTRUM_VERSION="$ROSTRUM_VERSION-$GIT_HEAD"
    - mkdir -p output
    - gzip -c target/release/rostrum > "output/rostrum-nexa-$ROSTRUM_VERSION.gz"
    - echo
      "<html><body><a href=\"rostrum-nexa-$ROSTRUM_VERSION.gz\">
        rostrum-nexa-$ROSTRUM_VERSION.gz
      </a></body></html>"
      > output/index.html
  artifacts:
    paths:
      - output


pages:
  image: alpine:latest
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
  dependencies:
    - generate-rustdoc
    - generate-mkdocs
    - archive-builds
  needs: [generate-rustdoc, generate-mkdocs, archive-builds]
  script:
    # from the generate-mkdocs job
    - mv build-docs public
    # from the generate-rustdoc job
    - mv target/doc public/reference
    # from the archive-builds job
    - mv output public/nightly
  artifacts:
    paths:
      - public
  cache: {}
