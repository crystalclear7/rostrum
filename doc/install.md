# Installation

## Signed binaries
Pre-built binaries exist for 64-bit GNU/Linux.

Stable versions are bundled with [Bitcoin Unlimited releases](https://www.bitcoinunlimited.info/download). These are deterministically built and signed by multiple individuals.

## Latest unsigned binaries

The latest binaries built by GitLab continuous integration as published [here](https://bitcoinunlimited.gitlab.io/rostrum/nightly).

Note that these are the latest build of the git repository, *here be dragons*.

## Install using cargo

Get cargo by installing a recent version of Rust. The tool [https://rustup.rs](rustup) is a good tool for installing Rust.

Run `cargo install rostrum` to install the latest release. This will build and install the latest version from [crates.io](https://crates.io/crates/rostrum).

Rostrum uses RocksDB and the requires clang and cmake. See [build documentation](build.md) to see how to install these packages on your platform.

## Building from source

See [build documentation](build.md).