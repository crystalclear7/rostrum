# Connecting to Rostrum

There are various ways to connect to your rostrum server, depending on your use case.

If you want to use the server privately for your wallet, Electron Cash is a popular wallet.

If you're using electrum for development purposes, the following options are suggested based on your choice of programming langauge:

| Language | Library |
|----------|---------|
| JavaScript / TypeScript | [electrum-cash](https://www.npmjs.com/package/electrum-cash) |
| Python | [bitcoincash](https://pypi.org/project/bitcoincash) |
| Android Kotlin | [libbitcoincashkotlin](https://gitlab.com/bitcoinunlimited/libbitcoincashkotlin) |

If you want to use it from a terminal, you can use the `contrib/client.py` script from the Rostrum source code repostiory.

## Wallet: Electron Cash

```bash
# Connecting to your local server
$ electron-cash --oneserver --server=127.0.0.1:50001:t
```

Note: Some versions of Electron Cash have an issue where it won't connect to non-SSL server, even when it's localhost.


## Terminal

Using the `contrib/client.py` script from the Rostrum source code repostiory.

```bash
$ python3 contrib/client.py -h
usage: client.py [-h] [--port PORT] [--server SERVER] method [args ...]

positional arguments:
  method
  args

optional arguments:
  -h, --help       show this help message and exit
  --port PORT
  --server SERVER
  ```

  Example:
  ```bash
% python3 contrib/client.py blockchain.block.headers 0 1
{'id': 0, 'jsonrpc': '2.0', 'result': {'count': 1, 'hex': '0100000000000000000000000000000000000000000000000000000000000000000000003ba3edfd7a7b12b27ac72c3e67768f617fc81bc3888a51323a9fb8aa4b1e5e4a29ab5f49ffff001d1dac2b7c', 'max': 2016}}
```

## JavaScript / TypeScript: ElectrumCash library

```typescript
import { ElectrumCluster, ElectrumTransport, RequestResponse } from 'electrum-cash';

electrum = new ElectrumCluster('client name', '1.4.2', 1, 1);
  electrum.addServer('bitcoincash.network');
  try {
    await electrum.ready();
  } catch (e) {
    console.log('Failed to connect ', e);
  }
  const response = await electrum.request(
      "blockchain.block.headers", 0, 1);
  await electrum.shutdown();
```
