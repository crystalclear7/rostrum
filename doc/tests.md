# Tests & Testing

## Unit tests
Run rostrum unit tests with `cargo test`.

## Integration tests

Rostrum has a large set of integration tests with the full node software [Bitcoin Unlimited](https://gitlab.com/bitcoinunlimited/BCHUnlimited).

The integration tests are located in the [qa/rpc-tests](https://gitlab.com/bitcoinunlimited/BCHUnlimited/-/tree/dev/qa/rpc-tests) folder of the node software and are prefixed with `electrum_` in their name.

This test set is run as part of Bitcoin Unlimited continuous integration to ensure that any changes to the node software does not break rostrum.

### Run tests from Rostrum

To run the tests from Rostrum, download [Bitcoin Unlimited](https://gitlab.com/bitcoinunlimited/BCHUnlimited), set $BITCOIN_HOME to the path of your download and starts the test with:

`BU_HOME=$HOME/BCHUnlimited ./contrib/run_functional_tests`

(Replace `$HOME/BCHUnlimited` with the correct path)

### Run tests from Bitcoin Unlimited

Clone and compile the [Bitcoin Unlimited](https://gitlab.com/bitcoinunlimited/BCHUnlimited) node software.

Start the electrum tests with `./qa/pull-tester/rpc-tests.py --electrum.exec="$HOME/rostrum/target/debug/rostrum" --electrum-only`.

The `--electrum.exec` argument is the path to the rostrum binary. The `--electrum-only` parameter is to skip any non-electrum integration test.
