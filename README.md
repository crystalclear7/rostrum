# Rostrum - Electrum Server in Rust

An efficient implementation of Electrum Server for Bitcoin Cash and Nexa.

![logo](https://gitlab.com/bitcoinunlimited/rostrum/-/raw/master/contrib/images/logo-platform-256x256.png)

### See [Documentation](https://bitcoinunlimited.gitlab.io/rostrum/)
### See [Protocol Methods](https://bitcoinunlimited.gitlab.io/rostrum/protocol/methods)

## Project

Rostrum is an efficient implementation of Electrum Server and can be used
as a drop-in replacement for ElectrumX. In addition to the TCP RPC interface,
it also provides WebSocket support.

Rostrum fully implements the
[v1.4.3 Electrum Cash protocol](https://bitcoinunlimited.gitlab.io/rostrum/protocol/methods)
plus many more queries.

The server indexes the entire blockchain, and the resulting index
enables fast queries for blockchain applications and any given user wallet,
allowing the user to keep real-time track of his balances and his transaction
history.

When run on the user's own machine, there is no need for the wallet to
communicate with external Electrum servers,
thus preserving the privacy of the user's addresses and balances.

## Features

- Supports Electrum protocol [v1.4.3](https://bitcoincash.network/electrum/)
- Maintains indexes over transaction inputs and outputs.
- Fast synchronization of the Bitcoin Cash blockchain on modest hardware
- `txindex` is not required for the Bitcoin node, however it does improve
  performance.
- Uses a single [RocksDB](https://github.com/spacejam/rust-rocksdb) database
  for better consistency and crash recovery.
- Has [really good integration with Bitcoin Unlimited](https://github.com/BitcoinUnlimited/BitcoinUnlimited/blob/release/doc/bu-electrum-integration.md).
- The best integration test coverage of all electrum server implementations.
  (see Tests section)

## Usage

See [here](https://bitcoinunlimited.gitlab.io/rostrum/usage/) for installation, build and usage instructions.

## Tests

Run unit tests with `cargo test`.

Rostrum has good integration test coverage with Bitcoin Unlimited full node. See the [testing documentation](
https://bitcoinunlimited.gitlab.io/rostrum/tests/)

## Linters

Code linters and formatting are checked with continuous integration before accepting chanages. When contributing, please run `cargo clippy` to catch common mistakes and
improve your code, as well as `cargo fmt` to format the code.
